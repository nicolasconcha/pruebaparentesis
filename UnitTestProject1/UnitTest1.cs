﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {

        private string[] tipoLlavesAbiertas = { "(", "{", "[" };
        private string[] tipoLlavesCerradas = { ")", "}", "]" };
        [TestMethod]
        public void TestMethod1()
        {

            List<string> cadenasEvaluar = new List<string>();
            cadenasEvaluar.Add("(2+3)*{2}");
            cadenasEvaluar.Add("asdasd");
            cadenasEvaluar.Add("()");
            cadenasEvaluar.Add("{{{}}");
            cadenasEvaluar.Add("({[)}]");
            foreach (var i in cadenasEvaluar)
            {
                var a = EvaluarCadena(i);
            }
        }

        //la base del metodo es agregar a la pila los caracteres abiertos y eliminarlos cuando encuentre el mismo pero cerrado
        private bool EvaluarCadena(string cadena)
        {
            if (!tienecaracteres(cadena))
                return true;// no tiene parentesis para evaluar
            var cadenalimpia = LimpiarCadena(cadena);
            var nuevacadena = cadenalimpia.ToArray();
            List<string> pila = new List<string>();
            bool bienCerrado = true;
            for (int i = 0; i < nuevacadena.Length; i++)
            {
                if (esAbierto(nuevacadena[i].ToString()))
                {
                    pila.Add(nuevacadena[i].ToString());
                }
                else
                {
                    if (!pila.Any())
                    {
                        bienCerrado = false;
                        break;
                    }
                    else if(pila.FirstOrDefault() != getContrario(nuevacadena[i]))
                        bienCerrado = false;
                    else
                        pila.RemoveAt(0);
                }
            }
            //si la pila tiene elementos es que no estan todos los parentesis bien cerrados 
            if (pila.Any())
            {
                bienCerrado = false;
            }
            
            return bienCerrado;


        }

        private bool esAbierto(string caracter)
        {
            var a = caracter.ToArray();
            for (int i = 0; i < tipoLlavesAbiertas.Length; i++)
            {
                if (tipoLlavesAbiertas[i] == caracter)
                    return true;
            }
            return false;
        }

        private string getContrario(char caracter)
        {
            for (int i = 0; i < tipoLlavesAbiertas.Length; i++)
            {
                if (tipoLlavesAbiertas[i] == caracter.ToString())
                    return tipoLlavesCerradas[i];
            }

            for (int i = 0; i < tipoLlavesCerradas.Length; i++)
            {
                if (tipoLlavesCerradas[i] == caracter.ToString())
                    return tipoLlavesAbiertas[i];
            }

            return string.Empty;
        }

        private string LimpiarCadena(string cadenaaLimpiar)
        {
            var cadenaseparada = cadenaaLimpiar.ToArray();
            var listaunificada = tipoLlavesAbiertas.Union(tipoLlavesCerradas).ToArray();
            foreach (var item in cadenaseparada)
            {
                if (!listaunificada.Contains(item.ToString()))
                    cadenaaLimpiar = cadenaaLimpiar.Replace(item.ToString(), "");
            }

            return cadenaaLimpiar;

        }

        private bool tienecaracteres(string cadena)
        {
            bool continecaracteres = false;
            for (int i = 0; i < cadena.Length; i++)
            {
                if (continecaracteres)
                    break;
                var listaunificada = tipoLlavesAbiertas.Union(tipoLlavesCerradas).ToArray();
                var caracter = cadena.ToArray()[i];
                for (int h = 0; h < listaunificada.Length; h++)
                {
                    if (caracter.ToString() == listaunificada[h])
                    {
                        continecaracteres = true;
                        break;
                    }
                }
            }

            return continecaracteres;
        }

        //private bool EvaluarCadena(string cadena)
        //{
        //    bool continecaracteres = false;
        //    for (int i = 0; i < cadena.Length; i++)
        //    {
        //        if (continecaracteres)
        //            break;
        //        var caracter = cadena.ToArray()[i];
        //        for (int h = 0; h < tipoLlaves.Length; h++)
        //        {
        //            if (caracter.ToString() == tipoLlaves[h])
        //            {
        //                continecaracteres = true;
        //                break;
        //            }
        //        }
        //    }
        //    //si no tiene los caracteres que buscamos retornamos un true
        //    if (!continecaracteres)
        //        return true; // no tiene caracteres de parentesis

        //    //limpiar cadena
        //    var cadenaLimpia = LimpiarCadena(cadena);

        //    //ahora se avalua de que esta todo cerrado
        //    if ((cadenaLimpia.Length % 2) == 0)
        //    {
        //        if (true)
        //        {

        //        }
        //        return true;
        //    }
        //    else
        //    {
        //        var primercaracter = cadenaLimpia.Substring(1);
        //        var ultimocaracter = cadenaLimpia.Substring(cadenaLimpia.Length);
        //    }

        //    return false;

        //}


    }
}
